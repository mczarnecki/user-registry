Requirements
----------------------------
1. JRE 8
2. JAVA_HOME environment set


Running the application
----------------------------
1. Build application and run tests:
cd user-registry
./mvnw clean package

2. Run application:
./mvnw spring-boot:run


Executing HTTP requests
----------------------------
1. Execute PUT request for valid user:
curl -H "Content-Type: application/json" -X PUT 'http://localhost:8080/register/user?username=User1&password=Abc123&dob=2010-10-10&ssn=421-92-0716'

2. Execute PUT request for invalid user:
curl -H "Content-Type: application/json" -X PUT 'http://localhost:8080/register/user?username=aaa&password=bbb&dob=ccc&ssn=ddd'

3.Execute PUT request for blacklisted user:
curl -H "Content-Type: application/json" -X PUT 'http://localhost:8080/register/user?username=User1&password=Abc123&dob=2010-10-10&ssn=111-92-0716'
