package com.mc.registry.rule;

import com.mc.registry.dto.User;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class UserNameValidationValidationRuleTest {
    private UserNameValidationValidationRule rule = new UserNameValidationValidationRule();

    @Test
    public void shouldNotValidateEmptySpace() {
        assertUserName("User name", false);
    }

    @Test
    public void shouldNotValidateSpecialCharacters() {
        assertUserName("aa!", false);
    }

    @Test
    public void shouldValidateAlphaNumeric() {
        assertUserName("Abc123", true);
    }

    private void assertUserName(String userName, boolean expectedResult) {
        //given
        User user = User.builder()
                .userName(userName)
                .build();
        //when
        boolean actualResult = rule.validate(user);

        //then
        assertEquals(expectedResult, actualResult);
    }

}