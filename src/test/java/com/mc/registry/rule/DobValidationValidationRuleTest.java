package com.mc.registry.rule;


import com.mc.registry.dto.User;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DobValidationValidationRuleTest {
    private DobValidationValidationRule rule = new DobValidationValidationRule();


    @Test
    public void ShouldNotValidateNonISODob() {
        assertDob("20101011", false);
        assertDob("05-05-2012", false);
        assertDob("junk", false);
    }

    @Test
    public void ShouldValidateISODob() {
        assertDob("2010-10-11", true);
    }

    private void assertDob(String dob, boolean expectedResult) {
        //given
        User user = User.builder()
                .dob(dob)
                .build();
        //when
        boolean actualResult = rule.validate(user);

        //then
        assertEquals(expectedResult, actualResult);
    }


}