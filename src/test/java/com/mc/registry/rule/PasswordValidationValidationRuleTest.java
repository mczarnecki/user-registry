package com.mc.registry.rule;

import com.mc.registry.dto.User;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PasswordValidationValidationRuleTest {
    private PasswordValidationValidationRule rule = new PasswordValidationValidationRule();

    @Test
    public void shouldNotValidateLessThen4Characters() {
        assertPassword("Abc", false);
        assertPassword("", false);
    }

    @Test
    public void shouldNotValidateMissingUpperCase() {
        assertPassword("password1234", false);
    }

    @Test
    public void shouldNotValidateMissingNumber() {
        assertPassword("Password", false);
    }

    @Test
    public void shouldValidateProperPassword() {
        assertPassword("Password1", true);
        assertPassword("Password 123", true);
    }

    private void assertPassword(String password, boolean expectedResult) {
        //given
        User user = User.builder()
                .password(password)
                .build();
        //when
        boolean actualResult = rule.validate(user);

        //then
        assertEquals(expectedResult, actualResult);
    }

}