package com.mc.registry.rule;

import com.mc.registry.dto.User;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SsnValidationValidationRuleTest {
    private SsnValidationValidationRule rule = new SsnValidationValidationRule();

    @Test
    public void ShouldValidateProperSsn() {
        assertSsn("153-06-9639", true);
        assertSsn("421-92-0716", true);
        assertSsn("111-92-0716", true);
    }

    @Test
    public void ShouldNotValidateMalformedSsn() {
        assertSsn("000-45-6789", false);
        assertSsn("153-06-0000", false);
        assertSsn("901-06-9639", false);
        assertSsn("153-061-9639", false);
        assertSsn("junk", false);
    }

    private void assertSsn(String ssn, boolean expectedResult) {
        //given
        User user = User.builder()
                .ssn(ssn)
                .build();
        //when
        boolean actualResult = rule.validate(user);

        //then
        assertEquals(expectedResult, actualResult);
    }

}