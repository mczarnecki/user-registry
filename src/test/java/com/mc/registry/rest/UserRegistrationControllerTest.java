package com.mc.registry.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mc.registry.AppConfig;
import com.mc.registry.dto.ErrorResponse;
import com.mc.registry.dto.SuccessResponse;
import com.mc.registry.dto.User;
import com.mc.registry.exception.AddUserException;
import com.mc.registry.service.RegistrationService;
import com.mc.registry.service.ValidatorService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
@WebAppConfiguration
public class UserRegistrationControllerTest {
    private MockMvc mvc;

    @InjectMocks
    private UserRegistrationController userRegistrationController;

    @Mock
    private RegistrationService registrationService;

    @Mock
    private ValidatorService validatorService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders
                .standaloneSetup(userRegistrationController)
                .setControllerAdvice(new UserRegistrationExceptionHandler())
                .build();
    }

    @Test
    public void shouldReturnProperHttpResponse() throws Exception {
        MvcResult mvcResult = this.mvc.perform(
                put("/register/user?username=User1&password=Abc123&dob=2010-10-10&ssn=421-92-0716"))
                .andExpect(status().isCreated())
                .andReturn();

        SuccessResponse expectedResponse = SuccessResponse.builder()
                .user(User.builder()
                        .userName("User1")
                        .password("Abc123")
                        .dob("2010-10-10")
                        .ssn("421-92-0716")
                        .build())
                .message(UserRegistrationController.USER_ADDED_SUCCESSFULLY)
                .status(HttpStatus.CREATED)
                .build();

        ObjectMapper mapper = new ObjectMapper();
        JsonNode responseJson = mapper.readTree(mvcResult.getResponse().getContentAsString());
        JsonNode expectedResponseJson = mapper.readTree(toJson(expectedResponse));

        assertEquals(expectedResponseJson, responseJson);
    }

    @Test
    public void shouldReturnErrorHttpResponse() throws Exception {
        doThrow(new AddUserException(RegistrationService.USER_BLACKLISTED_MSG))
                .when(registrationService).register(any());

        MvcResult mvcResult = this.mvc.perform(
                put("/register/user?username=User1&password=Abc123&dob=2010-10-10&ssn=111-92-0716"))
                .andExpect(status().isNotAcceptable())
                .andReturn();

        ErrorResponse expectedResponse = ErrorResponse.builder()
                .errorMessage(RegistrationService.USER_BLACKLISTED_MSG)
                .status(HttpStatus.NOT_ACCEPTABLE)
                .build();

        ObjectMapper mapper = new ObjectMapper();
        JsonNode responseJson = mapper.readTree(mvcResult.getResponse().getContentAsString());
        JsonNode expectedResponseJson = mapper.readTree(toJson(expectedResponse));

        assertEquals(expectedResponseJson, responseJson);
    }

    private static String toJson(final Object obj) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(obj);
    }

}