package com.mc.registry.service;

import com.mc.registry.AppConfig;
import com.mc.registry.dao.UserDao;
import com.mc.registry.dto.User;
import com.mc.registry.exception.AddUserException;
import com.mc.registry.exception.UserValidationException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
public class RegistrationServiceTest {
    @Autowired
    private RegistrationService registrationService;

    @Autowired
    private UserDao userDao;

    @Before
    public void init() {
        userDao.removeAllUsers();
    }

    @Test(expected = UserValidationException.class)
    public void shouldThrowExceptionForInvalidInput() throws AddUserException, UserValidationException {
        registrationService.register(getCorruptedUser());
    }

    @Test(expected = AddUserException.class)
    public void shouldThrowExceptionForBlacklistedUser() throws AddUserException, UserValidationException {
        registrationService.register(getBlacklistedUser());
    }

    @Test(expected = AddUserException.class)
    public void shouldThrowExceptionWhenUserAlreadyExists() throws AddUserException, UserValidationException {
        User user = getValidUser("UserName");
        registrationService.register(user);
        assertTrue(userDao.isUserExist(user));
        registrationService.register(user);
    }

    @Test
    public void shouldRegisterValidUsers() throws AddUserException, UserValidationException {
        registrationService.register(getValidUser("UserName1"));
        registrationService.register(getValidUser("UserName2"));
        registrationService.register(getValidUser("UserName3"));

        assertEquals(3, userDao.getUserCount());
    }

    private User getValidUser(String userName) {
        return User.builder()
                .userName(userName)
                .password("Pass1234!")
                .ssn("121-06-9639")
                .dob("2010-10-10")
                .build();
    }

    private User getCorruptedUser() {
        return User.builder()
                .userName("user Name")
                .password("")
                .ssn("153-06-9639")
                .dob("2010-10-10")
                .build();
    }

    private User getBlacklistedUser() {
        return User.builder()
                .userName("userName1")
                .password("Pass1234!")
                .ssn("111-06-9639")
                .dob("2010-10-10")
                .build();
    }

}