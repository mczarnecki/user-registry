package com.mc.registry.service;

import com.mc.registry.AppConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertTrue;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
public class ExclusionServiceTest {
    @Autowired
    ExclusionService exclusionService;

    @Test
    public void shouldNotValidate111Area(){
        assertTrue(!exclusionService.validate("any", "111-92-0716"));
    }

    @Test
    public void shouldValidateOtherAreas(){
        assertTrue(exclusionService.validate("any", "112-92-0716"));
        assertTrue(exclusionService.validate("any", "421-92-0716"));
    }

}