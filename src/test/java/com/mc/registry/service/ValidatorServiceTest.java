package com.mc.registry.service;


import com.mc.registry.AppConfig;
import com.mc.registry.dto.User;
import com.mc.registry.rule.DobValidationValidationRule;
import com.mc.registry.rule.PasswordValidationValidationRule;
import com.mc.registry.rule.SsnValidationValidationRule;
import com.mc.registry.rule.UserNameValidationValidationRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
public class ValidatorServiceTest {
    @Autowired
    private ValidatorService validatorService;

    @Test
    public void shouldNotResultInAnyErrors() {
        //given
        User user = User.builder()
                .userName("userName")
                .password("Abcd123!")
                .ssn("153-06-9639")
                .dob("2010-10-10")
                .build();

        //when
        List<String> errors = validatorService.validate(user);

        //then
        assertTrue(errors.isEmpty());
    }

    @Test
    public void shouldFailAll() {
        //given
        User user = User.builder()
                .userName("user Name")
                .password("123")
                .ssn("153-06")
                .dob("10-10-2011")
                .build();

        //when
        List<String> errors = validatorService.validate(user);

        //then
        assertEquals(4, errors.size());
        assertTrue(errors.contains(UserNameValidationValidationRule.ERROR_MESSAGE));
        assertTrue(errors.contains(PasswordValidationValidationRule.ERROR_MESSAGE));
        assertTrue(errors.contains(DobValidationValidationRule.ERROR_MESSAGE));
        assertTrue(errors.contains(SsnValidationValidationRule.ERROR_MESSAGE));
    }

}

