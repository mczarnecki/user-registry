package com.mc.registry.dao;

import com.mc.registry.AppConfig;
import com.mc.registry.dto.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
public class UserDaoTest {
    @Autowired
    private UserDao userDao;

    @Test
    public void ShouldSaveUser() {
        //given
        User user = User.builder()
                .userName("name")
                .dob("2000-10-10")
                .password("Password1")
                .ssn("ssn")
                .build();

        //when
        userDao.saveUser(user);

        //then
        assertTrue(userDao.isUserExist(user));
        assertEquals(1, userDao.getUserCount());
    }

}