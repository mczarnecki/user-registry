package com.mc.registry.dao;


import com.mc.registry.dto.User;

/**
 * Data Access Object for User
 */
public interface UserDao {

    /**
     * Saves user
     * @param user to be saved in repository
     * @return persisted User
     */
    User saveUser(User user);

    /**
     * Verifies if given user exists in repository
     * @param user
     * @return true if user exists, false otherwise
     */
    boolean isUserExist(User user);

    /**
     *
     * @return total user count
     */
    long getUserCount();

    /**
     * Removes all users
     */
    void removeAllUsers();

}
