package com.mc.registry.dao;


import com.mc.registry.dto.User;
import org.springframework.stereotype.Repository;

import java.util.concurrent.CopyOnWriteArraySet;

/**
 * User Data Access Object for memory storage
 */
@Repository
public class InMemoryUserDao implements UserDao {
    private CopyOnWriteArraySet<User> userRegistry = new CopyOnWriteArraySet<>();

    @Override
    public User saveUser(User user) {
        userRegistry.add(user);
        return user;
    }

    @Override
    public boolean isUserExist(User user) {
        return userRegistry.contains(user);
    }

    @Override
    public long getUserCount() {
        return userRegistry.size();
    }

    @Override
    public void removeAllUsers() {
        userRegistry.clear();
    }

}
