package com.mc.registry.rule;

import com.mc.registry.dto.User;

public class UserNameValidationValidationRule implements ValidationRule {
    public final static String ERROR_MESSAGE = "User name is not alphanumeric";

    @Override
    public boolean validate(final User user) {
       return user.getUserName().matches("^[a-zA-Z0-9]*$");
    }

    @Override
    public String getErrorMessage() {
        return ERROR_MESSAGE;
    }
}
