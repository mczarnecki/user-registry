package com.mc.registry.rule;

import com.mc.registry.dto.User;

public class PasswordValidationValidationRule implements ValidationRule {
    public final static String ERROR_MESSAGE = "Password does not match policy";

    @Override
    public boolean validate(final User user) {

        boolean result = true;
        if (user.getPassword().length() < 4 ||
                user.getPassword().equals(user.getPassword().toLowerCase()) ||
                !user.getPassword().matches(".*\\d+.*")) {
            result = false;
        }

        return result;
    }

    @Override
    public String getErrorMessage() {
        return ERROR_MESSAGE;
    }
}
