package com.mc.registry.rule;

import com.mc.registry.dto.User;

public class SsnValidationValidationRule implements ValidationRule {
    public final static String ERROR_MESSAGE = "Incorrect ssn";

    @Override
    public boolean validate(User user) {
        return user.getSsn().matches("^(?!(000|666|9))\\d{3}-(?!00)\\d{2}-(?!0000)\\d{4}$");
    }

    @Override
    public String getErrorMessage() {
        return ERROR_MESSAGE;
    }
}
