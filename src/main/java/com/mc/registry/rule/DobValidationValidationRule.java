package com.mc.registry.rule;

import com.mc.registry.dto.User;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class DobValidationValidationRule implements ValidationRule {
    public final static String ERROR_MESSAGE = "Incorrect date of bird format";

    @Override
    public boolean validate(final User user) {
        try {
            LocalDate.parse(user.getDob(), DateTimeFormatter.ISO_DATE);
        } catch (DateTimeParseException ex) {
            return false;
        }
        return true;
    }

    @Override
    public String getErrorMessage() {
        return ERROR_MESSAGE;
    }
}
