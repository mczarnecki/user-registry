package com.mc.registry.rule;


import com.mc.registry.dto.User;

public interface ValidationRule {
    boolean validate(final User user);

    String getErrorMessage();

}
