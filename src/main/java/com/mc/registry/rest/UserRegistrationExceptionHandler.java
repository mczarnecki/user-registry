package com.mc.registry.rest;


import com.mc.registry.exception.AddUserException;
import com.mc.registry.exception.UserValidationException;
import com.mc.registry.dto.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class UserRegistrationExceptionHandler {

    @ExceptionHandler(AddUserException.class)
    public ResponseEntity<ErrorResponse> handleAddUserException(AddUserException e) {
        ErrorResponse error = ErrorResponse.builder()
                .errorMessage(e.getMessage())
                .status(HttpStatus.NOT_ACCEPTABLE)
                .build();

        return new ResponseEntity<>(error, HttpStatus.NOT_ACCEPTABLE);
    }

    @ExceptionHandler(value = {MissingServletRequestParameterException.class, UserValidationException.class})
    protected ResponseEntity<ErrorResponse> handleBadRequest(Exception e) {
        ErrorResponse error = ErrorResponse.builder()
                .status(HttpStatus.BAD_REQUEST)
                .errorMessage(e.getMessage())
                .build();

        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {Exception.class})
    protected ResponseEntity<ErrorResponse> handleGeneralException(Exception e) {
        ErrorResponse error = ErrorResponse.builder()
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .errorMessage(e.getMessage())
                .build();

        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
