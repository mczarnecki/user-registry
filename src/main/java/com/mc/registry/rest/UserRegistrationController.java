package com.mc.registry.rest;

import com.mc.registry.dto.SuccessResponse;
import com.mc.registry.dto.User;
import com.mc.registry.exception.AddUserException;
import com.mc.registry.exception.UserValidationException;
import com.mc.registry.service.RegistrationService;
import com.mc.registry.service.ValidatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserRegistrationController {
    public final static String USER_ADDED_SUCCESSFULLY = "User Added successfully";

    private RegistrationService registrationService;
    private ValidatorService validatorService;

    @Autowired
    public UserRegistrationController(RegistrationService registrationService, ValidatorService validatorService) {
        this.registrationService = registrationService;
        this.validatorService = validatorService;
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/register/user")
    public ResponseEntity<SuccessResponse> register(@RequestParam(value = "username") String username,
                                                    @RequestParam(value = "password") String password,
                                                    @RequestParam(value = "dob") String dob,
                                                    @RequestParam(value = "ssn") String ssn)
            throws AddUserException, UserValidationException {

        User user = User.builder()
                .userName(username)
                .password(password)
                .dob(dob)
                .ssn(ssn)
                .build();

        validatorService.validate(user);
        registrationService.register(user);

        return new ResponseEntity<>(SuccessResponse.builder()
                .user(user)
                .message(USER_ADDED_SUCCESSFULLY)
                .status(HttpStatus.CREATED)
                .build(),
                HttpStatus.CREATED);
    }

}
