package com.mc.registry.service;


import com.mc.registry.dto.User;
import com.mc.registry.rule.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Service to validate URL input data
 */
@Service
public class ValidatorService {
    /**
     *
     * Validates user data against following policy:
     * - username (alphanumerical, no spaces)
     * - password (at least four characters, at least one upper case character, at least one number)
     * - date of birth (ISO 8601)
     * - Social Security Number (SSN)
     *
     * @param user to be validated
     * @return true if user data matches all validation rules, false otherwise
     */
    public List<String> validate(final User user) {
        List<String> errors = new ArrayList<>();
        List<ValidationRule> validationRules = new ArrayList<>();

        validationRules.add(new UserNameValidationValidationRule());
        validationRules.add(new PasswordValidationValidationRule());
        validationRules.add(new DobValidationValidationRule());
        validationRules.add(new SsnValidationValidationRule());

        validationRules.forEach(rule-> {
            if (!rule.validate(user)) {
                errors.add(rule.getErrorMessage());
            }
        });

        return errors;
    }

}
