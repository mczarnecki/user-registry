package com.mc.registry.service;

import org.springframework.stereotype.Service;

/**
 * This is mock implementation of Exclusion Service. It fails validation for all users with SSN with "111-" prefix
 */
@Service
public class FakeExclusiveService implements ExclusionService {
    private final static String BLACK_LISTED_SSN_AREA = "111-";

    @Override
    public boolean validate(String dob, String ssn) {
        return ssn.indexOf(BLACK_LISTED_SSN_AREA) < 0;
    }

}
