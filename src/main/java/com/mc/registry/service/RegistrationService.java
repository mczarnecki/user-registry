package com.mc.registry.service;

import com.mc.registry.dao.UserDao;
import com.mc.registry.dto.User;
import com.mc.registry.exception.AddUserException;
import com.mc.registry.exception.UserValidationException;
import com.mc.registry.service.ExclusionService;
import com.mc.registry.service.RegistrationService;
import com.mc.registry.service.ValidatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class RegistrationService {

    public static final String USER_BLACKLISTED_MSG = "User is blacklisted";
    public static final String USER_ALREADY_EXISTS_MSG = "User already exists";

    private UserDao userDao;
    private ExclusionService exclusionService;
    private ValidatorService validatorService;

    @Autowired
    public RegistrationService(ExclusionService exclusionService,
                               ValidatorService validatorService,
                               UserDao userDao) {
        this.exclusionService = exclusionService;
        this.validatorService = validatorService;
        this.userDao = userDao;
    }

    public void register(final User user) throws AddUserException, UserValidationException {
        List<String> validationErrors = validatorService.validate(user);
        if (!validationErrors.isEmpty()) {
            throw new UserValidationException(validationErrors.toString());
        }
        if (!exclusionService.validate(user.getDob(), user.getSsn())) {
            throw new AddUserException(USER_BLACKLISTED_MSG);
        }
        if (userDao.isUserExist(user)) {
            throw new AddUserException(USER_ALREADY_EXISTS_MSG);
        }
        userDao.saveUser(user);
    }

}

