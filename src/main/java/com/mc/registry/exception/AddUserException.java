package com.mc.registry.exception;


public class AddUserException extends Exception {
    public AddUserException(String message) {
        super(message);
    }
}
