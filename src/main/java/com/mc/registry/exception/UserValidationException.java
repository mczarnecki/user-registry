package com.mc.registry.exception;

public class UserValidationException extends Exception{
    public UserValidationException(String message) {
        super(message);
    }
}
