package com.mc.registry.dto;

import lombok.*;

@Builder
@Getter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class User {
    public String userName;
    public String password;
    public String dob;
    public String ssn;

}
