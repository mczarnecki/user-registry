package com.mc.registry.dto;

import lombok.Builder;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Builder
@Getter  //required by Jaxon
public class ErrorResponse {
    HttpStatus status;
    String errorMessage;
}
